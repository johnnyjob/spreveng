﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunCRC.Crc.Crc8;
using SunCRC.CrcGeneric;


namespace SunCRC.CrcTest.Crc8Test
{
    class Crc8Test
    {
        public class Crc8TestData
        {
            public Crc8Model CrcModel { get; private set; }
            public byte[] InputBytes { get; private set; }
            public byte ResultCrc { get; private set; }

            public Crc8TestData(Crc8Model model, byte[] inputbytes, byte result)
            {
                this.CrcModel = model;
                this.InputBytes = inputbytes;
                this.ResultCrc = result;
            }
        }
        
        private static Crc8TestData[] testdataEntries = new Crc8TestData[]  
        {
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850_ZERO, new byte[] { 0xC2 }, 0x0F),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850_ZERO, new byte[] { 0x01, 0x02, 0x99, 0xFF, 0xFE, 0x81}, 0xBB),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850_ZERO, new byte[] { 0xFF, 0x02, 0x08 }, 0x7E),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850_ZERO, new byte[] { 0xFF, 0x02, 0x07 }, 0xC5),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850_ZERO, new byte[] { 0xFF, 0x02, 0x09 }, 0x63),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850_ZERO, new byte[] { 0xFF, 0x02, 0x00 }, 0x96),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850_ZERO, new byte[] { 0xFF, 0x02 }, 0x7B),

            new Crc8TestData(Crc8Database.CRC8_SAE_J1850, CrcTestUtil.StandardTestDataInput, 0x4B),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850, new byte[] { 0x00, 0x00, 0x00, 0x00 }, 0x59),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850, new byte[] { 0xF2, 0x01, 0x83  }, 0x37),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850, new byte[] { 0x0F, 0xAA, 0x00, 0x55 }, 0x79),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850, new byte[] { 0x00, 0xFF, 0x55, 0x11 }, 0xB8),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850, new byte[] { 0x33, 0x22, 0x55, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF }, 0xCB),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850, new byte[] { 0x92, 0x6B, 0x55 }, 0x8C),
            new Crc8TestData(Crc8Database.CRC8_SAE_J1850, new byte[] { 0xFF, 0xFF, 0xFF, 0xFF }, 0x74),

            new Crc8TestData(Crc8Database.CRC8, CrcTestUtil.StandardTestDataInput, 0xF4),

            new Crc8TestData(Crc8Database.CRC8_8H2F, CrcTestUtil.StandardTestDataInput, 0xDF),
            new Crc8TestData(Crc8Database.CRC8_8H2F, new byte[] { 0x00, 0x00, 0x00, 0x00 }, 0x12),
            new Crc8TestData(Crc8Database.CRC8_8H2F, new byte[] { 0xF2, 0x01, 0x83  }, 0xC2),
            new Crc8TestData(Crc8Database.CRC8_8H2F, new byte[] { 0x0F, 0xAA, 0x00, 0x55 }, 0xC6),
            new Crc8TestData(Crc8Database.CRC8_8H2F, new byte[] { 0x00, 0xFF, 0x55, 0x11 }, 0x77),
            new Crc8TestData(Crc8Database.CRC8_8H2F, new byte[] { 0x33, 0x22, 0x55, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF }, 0x11),
            new Crc8TestData(Crc8Database.CRC8_8H2F, new byte[] { 0x92, 0x6B, 0x55 }, 0x33),
            new Crc8TestData(Crc8Database.CRC8_8H2F, new byte[] { 0xFF, 0xFF, 0xFF, 0xFF }, 0x6C),

            new Crc8TestData(Crc8Database.CRC8_CDMA2000, CrcTestUtil.StandardTestDataInput, 0xDA),
            new Crc8TestData(Crc8Database.CRC8_DARC, CrcTestUtil.StandardTestDataInput, 0x15),
            new Crc8TestData(Crc8Database.CRC8_DVB_S2, CrcTestUtil.StandardTestDataInput, 0xBC),
            new Crc8TestData(Crc8Database.CRC8_EBU, CrcTestUtil.StandardTestDataInput, 0x97),
            new Crc8TestData(Crc8Database.CRC8_ICODE, CrcTestUtil.StandardTestDataInput, 0x7E),
            new Crc8TestData(Crc8Database.CRC8_ITU, CrcTestUtil.StandardTestDataInput, 0xA1),
            new Crc8TestData(Crc8Database.CRC8_MAXIM, CrcTestUtil.StandardTestDataInput, 0xA1),
            new Crc8TestData(Crc8Database.CRC8_ROHC, CrcTestUtil.StandardTestDataInput, 0xD0),
            new Crc8TestData(Crc8Database.CRC8_WCDMA, CrcTestUtil.StandardTestDataInput, 0x25)
        };

        public void RunTests()
        {
            RunManualTests();
            RunStandardCrcTests();
            RunGenericCrcTests();
        }

        private void RunStandardCrcTests()
        {
            Console.Out.WriteLine("\nGeneral tests using ShiftReg method:");
            foreach (Crc8TestData testEntry in testdataEntries)
            {
                Crc8 crcAlgo = new Crc8(testEntry.CrcModel);
                CrcTestUtil.Assert8(crcAlgo.Compute_Simple_ShiftReg(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Simple method:");
            foreach (Crc8TestData testEntry in testdataEntries)
            {
                Crc8 crcAlgo = new Crc8(testEntry.CrcModel);
                CrcTestUtil.Assert8(crcAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Table method:");
            foreach (Crc8TestData testEntry in testdataEntries)
            {
                Crc8 crcAlgo = new Crc8(testEntry.CrcModel);
                CrcTestUtil.Assert8(crcAlgo.Compute(testEntry.InputBytes), testEntry.ResultCrc);
            }

            // Reflected ones
            Console.Out.WriteLine("\nGeneral tests using ShiftReg method (Reflected):");
            foreach (Crc8TestData testEntry in testdataEntries)
            {
                Crc8Reflected crcAlgo = new Crc8Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert8(crcAlgo.Compute_Simple_ShiftReg(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Simple method (Reflected):");
            foreach (Crc8TestData testEntry in testdataEntries)
            {
                Crc8Reflected crcAlgo = new Crc8Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert8(crcAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Table method (Reflected):");
            foreach (Crc8TestData testEntry in testdataEntries)
            {
                Crc8Reflected crcAlgo = new Crc8Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert8(crcAlgo.Compute(testEntry.InputBytes), testEntry.ResultCrc);
            }
        }

        private void RunGenericCrcTests()
        {
            Console.Out.WriteLine("\nGeneral tests using CrcGeneric class:");
            foreach (Crc8TestData testEntry in testdataEntries)
            {
                CrcGenericImpl<byte> crcGenAlgo = CrcGenericFactory.GetGenericCrc8(testEntry.CrcModel);
                CrcTestUtil.Assert8(crcGenAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }
        }

        private void RunManualTests()
        {
            Console.Out.WriteLine("Manual tests:");
            Crc8 crc8 = new Crc8(Crc8Database.CRC8_SAE_J1850_ZERO);
            CrcTestUtil.Assert8(crc8.Compute_Simple_ShiftReg_OneByte(0xC2), 0x0F);
            CrcTestUtil.Assert8(crc8.Compute_Simple_OneByte(0xC2), 0x0F);

            //Crc8 crc81 = new Crc8(Crc8Database.CRC8);
            //CrcTestUtil.Assert8(crc81.Compute_Simple_ShiftReg_OneByte(0xC2), 0x0F);
            //CrcTestUtil.Assert8(crc81.Compute_Simple_ShiftReg(new byte[] { 0xC2 }), 0x0F);

            //Crc8 crc8test = new Crc8(Crc8Database.CRC8);
            //CrcTestUtil.Assert8(crc8test.Compute_Simple_ShiftReg_OneByte(0x01), 0x0F);
            //CrcTestUtil.Assert8(crc8test.Compute_Simple_ShiftReg(new byte[] { 0x01, 0x4, 0x5 }), 0x0F);
            //CrcTestUtil.Assert8(crc8test.Compute_Simple(new byte[] { 0x01, 0x4, 0x5 }), 0x0F);
            //CrcTestUtil.Assert8(crc8test.Compute(new byte[] { 0x01, 0x4, 0x5 }), 0x0F);
        }
    }
}
