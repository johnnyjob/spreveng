﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunCRC.Crc.Crc32;
using SunCRC.CrcGeneric;

namespace SunCRC.CrcTest.Crc32Test
{
    class Crc32Test
    {
        public class Crc32TestData
        {
            public Crc32Model CrcModel { get; private set; }
            public byte[] InputBytes { get; private set; }
            public uint ResultCrc { get; private set; }

            public Crc32TestData(Crc32Model model, byte[] inputbytes, uint result)
            {
                this.CrcModel = model;
                this.InputBytes = inputbytes;
                this.ResultCrc = result;
            }
        }

        private static Crc32TestData[] testdataEntries = new Crc32TestData[]  
        {
            new Crc32TestData(Crc32Database.CRC32, CrcTestUtil.StandardTestDataInput, 0xCBF43926),
            new Crc32TestData(Crc32Database.CRC32_BZIP2, CrcTestUtil.StandardTestDataInput, 0xFC891918),
            new Crc32TestData(Crc32Database.CRC32_C, CrcTestUtil.StandardTestDataInput, 0xe3069283),
            new Crc32TestData(Crc32Database.CRC32_D, CrcTestUtil.StandardTestDataInput, 0x87315576),
            new Crc32TestData(Crc32Database.CRC32_MPEG2, CrcTestUtil.StandardTestDataInput, 0x0376E6E7),
            new Crc32TestData(Crc32Database.CRC32_POSIX, CrcTestUtil.StandardTestDataInput, 0x765E7680),
            new Crc32TestData(Crc32Database.CRC32_Q, CrcTestUtil.StandardTestDataInput, 0x3010BF7F),
            new Crc32TestData(Crc32Database.CRC32_JAMCRC, CrcTestUtil.StandardTestDataInput, 0x340BC6D9),
            new Crc32TestData(Crc32Database.CRC32_XFER, CrcTestUtil.StandardTestDataInput,0xBD0BE338)
        };

        public void RunTests()
        {
            RunManualTests();
            RunStandardCrcTests();
            RunGenericCrcTests();
        }

        private void RunManualTests()
        {
            Console.Out.WriteLine("Manual tests:");
            Crc32 crcAlgo = new Crc32(Crc32Database.CRC32);
            CrcTestUtil.Assert32(crcAlgo.Compute_Simple_ShiftReg(new byte[] { 0xC2 }), 0xA7684C11);
            CrcTestUtil.Assert32(crcAlgo.Compute_Simple(new byte[] { 0xC2 }), 0xA7684C11);
            CrcTestUtil.Assert32(crcAlgo.Compute(new byte[] { 0xC2 }), 0xA7684C11);
        }

        private void RunStandardCrcTests()
        {
            Console.Out.WriteLine("\nGeneral tests using ShiftReg method:");
            foreach (Crc32TestData testEntry in testdataEntries)
            {
                Crc32 crcAlgo = new Crc32(testEntry.CrcModel);
                CrcTestUtil.Assert32(crcAlgo.Compute_Simple_ShiftReg(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Simple method:");
            foreach (Crc32TestData testEntry in testdataEntries)
            {
                Crc32 crcAlgo = new Crc32(testEntry.CrcModel);
                CrcTestUtil.Assert32(crcAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Table method:");
            foreach (Crc32TestData testEntry in testdataEntries)
            {
                Crc32 crcAlgo = new Crc32(testEntry.CrcModel);
                CrcTestUtil.Assert32(crcAlgo.Compute(testEntry.InputBytes), testEntry.ResultCrc);
            }

            // Reflected ones
            Console.Out.WriteLine("\nGeneral tests using ShiftReg method (Reflected):");
            foreach (Crc32TestData testEntry in testdataEntries)
            {
                Crc32Reflected crcAlgo = new Crc32Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert32(crcAlgo.Compute_Simple_ShiftReg(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Simple method (Reflected):");
            foreach (Crc32TestData testEntry in testdataEntries)
            {
                Crc32Reflected crcAlgo = new Crc32Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert32(crcAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Table method (Reflected):");
            foreach (Crc32TestData testEntry in testdataEntries)
            {
                Crc32Reflected crcAlgo = new Crc32Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert32(crcAlgo.Compute(testEntry.InputBytes), testEntry.ResultCrc);
            }
        }

        private void RunGenericCrcTests()
        {
            Console.Out.WriteLine("\nGeneral tests using CrcGeneric class:");
            foreach (Crc32TestData testEntry in testdataEntries)
            {
                CrcGenericImpl<uint> crcGenAlgo = CrcGenericFactory.GetGenericCrc32(testEntry.CrcModel);
                CrcTestUtil.Assert32(crcGenAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }
        }
    }
}
