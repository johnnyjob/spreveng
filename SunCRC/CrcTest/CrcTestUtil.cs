﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.CrcTest
{
    public class CrcTestUtil
    {
        public static readonly byte[] StandardTestDataInput = new byte[] { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39 };

        public static bool Assert8(byte actualValue, byte expectedValue)
        {
            if (actualValue != expectedValue)
            {
                LogFailedTest(actualValue, expectedValue);
                return false;
            }
            else
            {
                LogSucceededTest();
                return true;
            }
        }

        public static bool Assert16(ushort actualValue, ushort expectedValue)
        {
            if (actualValue != expectedValue)
            {
                LogFailedTest(actualValue, expectedValue);
                return false;
            }
            else
            {
                LogSucceededTest();
                return true;
            }
        }

        public static bool Assert32(uint actualValue, uint expectedValue)
        {
            if (actualValue != expectedValue)
            {
                LogFailedTest(actualValue, expectedValue);
                return false;
            }
            else
            {
                LogSucceededTest();
                return true;
            }
        }

        public static bool Assert64(ulong actualValue, ulong expectedValue)
        {
            if (actualValue != expectedValue)
            {
                LogFailedTest(actualValue, expectedValue);
                return false;
            }
            else
            {
                LogSucceededTest();
                return true;
            }
        }

        public static void LogFailedTest(byte actualValue, byte expectedValue)
        {
            Console.Out.WriteLine("");
            Console.Out.WriteLine("F: Actual: " + StringUtil.ToHex(actualValue) + " Expected: " + StringUtil.ToHex(expectedValue));
        }

        public static void LogFailedTest(ushort actualValue, ushort expectedValue)
        {
            Console.Out.WriteLine("");
            Console.Out.WriteLine("F: Actual: " + StringUtil.ToHex(actualValue) + " Expected: " + StringUtil.ToHex(expectedValue));
        }

        public static void LogFailedTest(uint actualValue, uint expectedValue)
        {
            Console.Out.WriteLine("");
            Console.Out.WriteLine("F: Actual: " + StringUtil.ToHex(actualValue) + " Expected: " + StringUtil.ToHex(expectedValue));
        }

        public static void LogFailedTest(ulong actualValue, ulong expectedValue)
        {
            Console.Out.WriteLine("");
            Console.Out.WriteLine("F: Actual: " + StringUtil.ToHex(actualValue) + " Expected: " + StringUtil.ToHex(expectedValue));
        }

        public static void LogSucceededTest()
        {
            Console.Out.Write("x");
        }
    }
}
