﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunCRC.Crc.Crc16;
using SunCRC.CrcGeneric;

namespace SunCRC.CrcTest.Crc16Test
{
    class Crc16Test
    {
        public class Crc16TestData
        {
            public Crc16Model CrcModel { get; private set; }
            public byte[] InputBytes { get; private set; }
            public ushort ResultCrc { get; private set; }

            public Crc16TestData(Crc16Model model, byte[] inputbytes, ushort result)
            {
                this.CrcModel = model;
                this.InputBytes = inputbytes;
                this.ResultCrc = result;
            }
        }

        private static Crc16TestData[] testdataEntries = new Crc16TestData[]  
        {
            new Crc16TestData(Crc16Database.CRC16_ARC, CrcTestUtil.StandardTestDataInput, 0xBB3D),
            new Crc16TestData(Crc16Database.CRC16_AUG_CCITT, CrcTestUtil.StandardTestDataInput, 0xE5CC),
            new Crc16TestData(Crc16Database.CRC16_BUYPASS, CrcTestUtil.StandardTestDataInput, 0xFEE8),
            new Crc16TestData(Crc16Database.CRC16_CCITT_FALSE, CrcTestUtil.StandardTestDataInput, 0x29B1),
            new Crc16TestData(Crc16Database.CRC16_CDMA2000, CrcTestUtil.StandardTestDataInput, 0x4C06),
            new Crc16TestData(Crc16Database.CRC16_DDS_110, CrcTestUtil.StandardTestDataInput, 0x9ECF),
            new Crc16TestData(Crc16Database.CRC16_DECT_R, CrcTestUtil.StandardTestDataInput, 0x007E),
            new Crc16TestData(Crc16Database.CRC16_DECT_X, CrcTestUtil.StandardTestDataInput, 0x007F),
            new Crc16TestData(Crc16Database.CRC16_DNP, CrcTestUtil.StandardTestDataInput, 0xEA82),
            new Crc16TestData(Crc16Database.CRC16_EN_13757, CrcTestUtil.StandardTestDataInput, 0xC2B7),
            new Crc16TestData(Crc16Database.CRC16_GENIBUS, CrcTestUtil.StandardTestDataInput, 0xD64E),
            new Crc16TestData(Crc16Database.CRC16_MAXIM, CrcTestUtil.StandardTestDataInput, 0x44C2),
            new Crc16TestData(Crc16Database.CRC16_MCRF4XX, CrcTestUtil.StandardTestDataInput, 0x6F91),
            new Crc16TestData(Crc16Database.CRC16_RIELLO, CrcTestUtil.StandardTestDataInput, 0x63D0),
            new Crc16TestData(Crc16Database.CRC16_T10_DIF, CrcTestUtil.StandardTestDataInput, 0xD0DB),
            new Crc16TestData(Crc16Database.CRC16_TELEDISK, CrcTestUtil.StandardTestDataInput, 0x0FB3), 
            new Crc16TestData(Crc16Database.CRC16_TMS37157, CrcTestUtil.StandardTestDataInput, 0x26B1), 
            new Crc16TestData(Crc16Database.CRC16_USB, CrcTestUtil.StandardTestDataInput, 0xB4C8),
            new Crc16TestData(Crc16Database.CRC16_A, CrcTestUtil.StandardTestDataInput, 0xBF05),
            new Crc16TestData(Crc16Database.CRC16_KERMIT, CrcTestUtil.StandardTestDataInput, 0x2189),
            new Crc16TestData(Crc16Database.CRC16_MODBUS, CrcTestUtil.StandardTestDataInput, 0x4B37),
            new Crc16TestData(Crc16Database.CRC16_X_25, CrcTestUtil.StandardTestDataInput, 0x906E),
            new Crc16TestData(Crc16Database.CRC16_XMODEM, CrcTestUtil.StandardTestDataInput, 0x31C3),
        };

        public void RunTests()
        {
            RunManualTests();
            RunStandardCrcTests();
            RunGenericCrcTests();
        }

        private void RunManualTests()
        {
            Console.Out.WriteLine("Manual tests:");
            Crc16 crc16 = new Crc16(Crc16Database.CRC16_CCIT_ZERO);
            CrcTestUtil.Assert16(crc16.Compute_Simple_ShiftReg(new byte[] { 0xC2 }), 0xF90E);
            CrcTestUtil.Assert16(crc16.Compute_Simple(new byte[] { 0xC2 }), 0xF90E);
            CrcTestUtil.Assert16(crc16.Compute_Simple(new byte[] { 0x01, 0x02, 0x99, 0xFF, 0xFE, 0x81 }), 0x4ACF);
            CrcTestUtil.Assert16(crc16.Compute_Simple(new byte[] { 0x01, 0x02 }), 0x1373);

            CrcTestUtil.Assert16(crc16.Compute(new byte[] { 0xC2 }), 0xF90E);
            CrcTestUtil.Assert16(crc16.Compute(new byte[] { 0x01, 0x02, 0x99, 0xFF, 0xFE, 0x81 }), 0x4ACF);
            CrcTestUtil.Assert16(crc16.Compute(new byte[] { 0x01, 0x02 }), 0x1373);

            Crc16 crc16test = new Crc16(Crc16Database.CRC16_AUG_CCITT);
            CrcTestUtil.Assert16(crc16test.Compute_Simple_ShiftReg(new byte[] { 21,12 }), 0xB9CA);
            CrcTestUtil.Assert16(crc16test.Compute_Simple(new byte[] { 21, 12 }), 0xB9CA);
            CrcTestUtil.Assert16(crc16test.Compute(new byte[] { 21, 12 }), 0xB9CA);
        }

        private void RunStandardCrcTests()
        {
            Console.Out.WriteLine("\nGeneral tests using ShiftReg method:");
            foreach (Crc16TestData testEntry in testdataEntries)
            {
                Crc16 crcAlgo = new Crc16(testEntry.CrcModel);
                CrcTestUtil.Assert16(crcAlgo.Compute_Simple_ShiftReg(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Simple method:");
            foreach (Crc16TestData testEntry in testdataEntries)
            {
                Crc16 crcAlgo = new Crc16(testEntry.CrcModel);
                CrcTestUtil.Assert16(crcAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Table method:");
            foreach (Crc16TestData testEntry in testdataEntries)
            {
                Crc16 crcAlgo = new Crc16(testEntry.CrcModel);
                CrcTestUtil.Assert16(crcAlgo.Compute(testEntry.InputBytes), testEntry.ResultCrc);
            }

            // Reflected ones
            Console.Out.WriteLine("\nGeneral tests using ShiftReg method (Reflected):");
            foreach (Crc16TestData testEntry in testdataEntries)
            {
                Crc16Reflected crcAlgo = new Crc16Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert16(crcAlgo.Compute_Simple_ShiftReg(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Simple method (Reflected):");
            foreach (Crc16TestData testEntry in testdataEntries)
            {
                Crc16Reflected crcAlgo = new Crc16Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert16(crcAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Table method (Reflected):");
            foreach (Crc16TestData testEntry in testdataEntries)
            {
                Crc16Reflected crcAlgo = new Crc16Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert16(crcAlgo.Compute(testEntry.InputBytes), testEntry.ResultCrc);
            }
        }


        private void RunGenericCrcTests()
        {
            Console.Out.WriteLine("\nGeneral tests using CrcGeneric class:");
            foreach (Crc16TestData testEntry in testdataEntries)
            {
                CrcGenericImpl<ushort> crcGenAlgo = CrcGenericFactory.GetGenericCrc16(testEntry.CrcModel);
                CrcTestUtil.Assert16(crcGenAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }
        }
    }
}
