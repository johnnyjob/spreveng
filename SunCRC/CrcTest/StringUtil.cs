﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.CrcTest
{
    class StringUtil
    {
        public static string ToHex(byte val)
        {
            return String.Format("0x{0:X2}", val);
        }

        public static string ToHex(ushort val)
        {
            return String.Format("0x{0:X4}", val);
        }

        public static string ToHex(uint val)
        {
            return String.Format("0x{0:X8}", val);
        }

        public static string ToHex(ulong val)
        {
            return String.Format("0x{0:X16}", val);
        }
    }
}
