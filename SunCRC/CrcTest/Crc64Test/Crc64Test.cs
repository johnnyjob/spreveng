﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunCRC.Crc.Crc64;
using SunCRC.CrcGeneric;

namespace SunCRC.CrcTest.Crc64Test
{
    class Crc64Test
    {
        public class Crc64TestData
        {
            public Crc64Model CrcModel { get; private set; }
            public byte[] InputBytes { get; private set; }
            public ulong ResultCrc { get; private set; }

            public Crc64TestData(Crc64Model model, byte[] inputbytes, ulong result)
            {
                this.CrcModel = model;
                this.InputBytes = inputbytes;
                this.ResultCrc = result;
            }
        }

        private static Crc64TestData[] testdataEntries = new Crc64TestData[]  
        {
            new Crc64TestData(Crc64Database.CRC64, CrcTestUtil.StandardTestDataInput, 0x6C40DF5F0B497347 ),
            new Crc64TestData(Crc64Database.CRC64_GO_ISO, CrcTestUtil.StandardTestDataInput, 0xB90956C775A41001 ),
            new Crc64TestData(Crc64Database.CRC64_WE, CrcTestUtil.StandardTestDataInput, 0x62EC59E3F1A4F00A ),
            new Crc64TestData(Crc64Database.CRC64_XZ, CrcTestUtil.StandardTestDataInput, 0x995DC9BBDF1939FA  )
        };

        public void RunTests()
        {
            RunManualTests();
            RunStandardCrcTests();
            RunGenericCrcTests();
        }

        private void RunManualTests()
        {
            /* no tests currently defined */
        }

        private void RunStandardCrcTests()
        {
            Console.Out.WriteLine("\nGeneral tests using ShiftReg method:");
            foreach (Crc64TestData testEntry in testdataEntries)
            {
                Crc64 crcAlgo = new Crc64(testEntry.CrcModel);
                CrcTestUtil.Assert64(crcAlgo.Compute_Simple_ShiftReg(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Simple method:");
            foreach (Crc64TestData testEntry in testdataEntries)
            {
                Crc64 crcAlgo = new Crc64(testEntry.CrcModel);
                CrcTestUtil.Assert64(crcAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Table method:");
            foreach (Crc64TestData testEntry in testdataEntries)
            {
                Crc64 crcAlgo = new Crc64(testEntry.CrcModel);
                CrcTestUtil.Assert64(crcAlgo.Compute(testEntry.InputBytes), testEntry.ResultCrc);
            }

            // Reflected ones
            Console.Out.WriteLine("\nGeneral tests using ShiftReg method (Reflected):");
            foreach (Crc64TestData testEntry in testdataEntries)
            {
                Crc64Reflected crcAlgo = new Crc64Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert64(crcAlgo.Compute_Simple_ShiftReg(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Simple method (Reflected):");
            foreach (Crc64TestData testEntry in testdataEntries)
            {
                Crc64Reflected crcAlgo = new Crc64Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert64(crcAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }

            Console.Out.WriteLine("\nGeneral tests using Table method (Reflected):");
            foreach (Crc64TestData testEntry in testdataEntries)
            {
                Crc64Reflected crcAlgo = new Crc64Reflected(testEntry.CrcModel);
                CrcTestUtil.Assert64(crcAlgo.Compute(testEntry.InputBytes), testEntry.ResultCrc);
            }
        }

        private void RunGenericCrcTests()
        {
            Console.Out.WriteLine("\nGeneral tests using CrcGeneric class:");
            foreach (Crc64TestData testEntry in testdataEntries)
            {
                CrcGenericImpl<ulong> crcGenAlgo = CrcGenericFactory.GetGenericCrc64(testEntry.CrcModel);
                CrcTestUtil.Assert64(crcGenAlgo.Compute_Simple(testEntry.InputBytes), testEntry.ResultCrc);
            }
        }
    }
}
