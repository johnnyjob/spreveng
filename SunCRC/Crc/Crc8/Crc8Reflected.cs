﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc8
{
    /// <summary>
    /// Different methods to compute a CRC8 value in the reflected way.
    /// </summary>
    public class Crc8Reflected
    {
        private Crc8Model crcModel;
        private byte[] crcTable;

        public Crc8Reflected(Crc8Model model)
        {
            this.crcModel = Crc8Model.GetReflectedCrcModel(model);
            CalculateCrcTable();
        }


        /// <summary>
        /// General CRC-8 algorithm close to the behavior of a shift register.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public byte Compute_Simple_ShiftReg(byte[] bytes)
        {
            byte crc = (byte)(crcModel.Initial ^ (crcModel.InputReflected ? CrcUtil.Reflect8(bytes[0]) : bytes[0]) );
            
            for (int byteIndex = 1; byteIndex < bytes.Length + 1; byteIndex++)
            {
                byte curByte = (byteIndex < bytes.Length) ? (crcModel.InputReflected ? CrcUtil.Reflect8(bytes[byteIndex]) : bytes[byteIndex]) : (byte)0;
                                
                for (int i = 0; i <= 7; i++)
                {
                    /* check if MSB is set */
                    if ((crc & 0x01) != 0)
                    {
                        crc = (byte)(crc >> 1);
                        crc = ((byte)(curByte & (1 << i)) != 0) ? (byte)(crc | 0x80) : (byte)(crc & 0x7F);
                        crc = (byte)(crc ^ crcModel.Polynomial);
                    }
                    else
                    {   /* MSB not set, shift it out and shift in next bit of input stream. Same as above, just no division */
                        crc = (byte)(crc >> 1);
                        crc = ((byte)(curByte & (1 << i)) != 0) ? (byte)(crc | 0x80) : (byte)(crc & 0x7F);
                    }
                }
            }
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect8(crc) : crc);
            return (byte)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// General CRC-8 simple algorithm.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public byte Compute_Simple(byte[] bytes)
        {
            byte crc = crcModel.Initial;

            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);
                crc ^= curByte; /* XOR-in the next input byte */

                for (int i = 0; i < 8; i++)
                {
                    if ((crc & 0x01) != 0)
                    {
                        crc = (byte)((crc >> 1) ^ crcModel.Polynomial);
                    }
                    else
                    {
                        crc >>= 1;
                    }
                }
            }
            
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect8(crc) : crc);
            return (byte)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// Precalculate CRC8 look up table.
        /// </summary>
        private void CalculateCrcTable()
        {
            crcTable = new byte[256];
            /* iterate over all byte values 0 - 255 */
            for (int divident = 0; divident < 256; divident++)
            {
                byte currByte = (byte)divident;
                /* calculate the CRC8 value for current byte */
                for (byte bit = 0; bit < 8; bit++)
                {
                    if ((currByte & 0x01) != 0)
                    {
                        currByte >>= 1;
                        currByte ^= crcModel.Polynomial;
                    }
                    else
                    {
                        currByte >>= 1;
                    }
                }
                /* store CRC value in lookup table */
                crcTable[divident] = currByte;
            }
        }

        /// <summary>
        /// Calculate CRC8 using lookup table.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public byte Compute(byte[] bytes)
        {
            byte crc = crcModel.Initial;
            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);

                /* XOR-in next input byte */
                byte data = (byte)(curByte ^ crc);
                /* get current CRC value = remainder */
                crc = (byte)(crcTable[data]);
            }
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect8(crc) : crc);
            return (byte)(crc ^ crcModel.FinalXor);
        }
    }
}
