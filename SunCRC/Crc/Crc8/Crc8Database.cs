﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc8
{
    /// <summary>
    /// Most information taken from http://reveng.sourceforge.net/crc-catalogue/
    /// </summary>
    class Crc8Database
    {
        public static Crc8Model CRC8 = new Crc8Model(0x07, 0x00, 0x00, false, false);
        public static Crc8Model CRC8_SAE_J1850 = new Crc8Model(0x1D, 0xFF, 0xFF, false, false);
        public static Crc8Model CRC8_SAE_J1850_ZERO = new Crc8Model(0x1D, 0x00, 0x00, false, false);
        public static Crc8Model CRC8_8H2F = new Crc8Model(0x2F, 0xFF, 0xFF, false, false);
        public static Crc8Model CRC8_CDMA2000 = new Crc8Model(0x9B, 0xFF, 0x00, false, false);
        public static Crc8Model CRC8_DARC = new Crc8Model(0x39, 0x00, 0x00, true, true);
        public static Crc8Model CRC8_DVB_S2 = new Crc8Model(0xD5, 0x00, 0x00, false, false);
        public static Crc8Model CRC8_EBU = new Crc8Model(0x1D, 0xFF, 0x00, true, true);
        public static Crc8Model CRC8_ICODE = new Crc8Model(0x1D, 0xFD, 0x00, false, false);
        public static Crc8Model CRC8_ITU = new Crc8Model(0x07, 0x00, 0x55, false, false);
        public static Crc8Model CRC8_MAXIM = new Crc8Model(0x31, 0x00, 0x00, true, true);
        public static Crc8Model CRC8_ROHC = new Crc8Model(0x07, 0xFF, 0x00, true, true);
        public static Crc8Model CRC8_WCDMA = new Crc8Model(0x9B, 0x00, 0x00, true, true);
    }
}
