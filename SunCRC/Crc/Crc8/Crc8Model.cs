﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc8
{
    /// <summary>
    /// Representing all required values to define a CRC-8
    /// </summary>
    public class Crc8Model
    {
        public byte Polynomial { get; private set; }
        public byte Initial { get; private set; }
        public byte FinalXor { get; private set; }
        public bool InputReflected { get; private set; }
        public bool ResultReflected { get; private set; }

        public Crc8Model(byte polynomial, byte initialVal, byte finalXorVal, bool inputReflected, bool resultReflected)
        {
            this.Polynomial = polynomial;
            this.Initial = initialVal;
            this.FinalXor = finalXorVal;
            this.InputReflected = inputReflected;
            this.ResultReflected = resultReflected;
        }

        public static Crc8Model GetReflectedCrcModel(Crc8Model model)
        {
            return new Crc8Model(
                CrcUtil.Reflect8(model.Polynomial),
                CrcUtil.Reflect8(model.Initial),
                model.FinalXor,
                !model.InputReflected,
                !model.ResultReflected
            );
        }
    }
}
