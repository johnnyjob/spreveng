﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc16
{
    /// <summary>
    /// Information taken from http://reveng.sourceforge.net/crc-catalogue/
    /// </summary>
    class Crc16Database
    {
        public static Crc16Model CRC16_CCIT_ZERO = new Crc16Model(0x1021, 0x0000, 0x0000, false, false);
    
        public static Crc16Model CRC16_ARC = new Crc16Model(0x8005, 0x0000, 0x0000, true, true);
        public static Crc16Model CRC16_AUG_CCITT = new Crc16Model(0x1021, 0x1D0F, 0x0000, false, false);
        public static Crc16Model CRC16_BUYPASS = new Crc16Model(0x8005, 0x0000, 0x0000, false, false);
        public static Crc16Model CRC16_CCITT_FALSE = new Crc16Model(0x1021, 0xFFFF, 0x0000, false, false);
        public static Crc16Model CRC16_CDMA2000 = new Crc16Model(0xC867, 0xFFFF, 0x0000, false, false);
        public static Crc16Model CRC16_DDS_110 = new Crc16Model(0x8005, 0x800D, 0x0000, false, false);
        public static Crc16Model CRC16_DECT_R = new Crc16Model(0x0589, 0x0000, 0x0001, false, false);
        public static Crc16Model CRC16_DECT_X = new Crc16Model(0x0589, 0x0000, 0x0000, false, false);
        public static Crc16Model CRC16_DNP = new Crc16Model(0x3D65, 0x0000, 0xFFFF, true, true);
        public static Crc16Model CRC16_EN_13757 = new Crc16Model(0x3D65, 0x0000, 0xFFFF, false, false);
        public static Crc16Model CRC16_GENIBUS = new Crc16Model(0x1021, 0xFFFF, 0xFFFF, false, false);
        public static Crc16Model CRC16_MAXIM = new Crc16Model(0x8005, 0x0000, 0xFFFF, true, true);
        public static Crc16Model CRC16_MCRF4XX = new Crc16Model(0x1021, 0xFFFF, 0x0000, true, true);
        public static Crc16Model CRC16_RIELLO = new Crc16Model(0x1021, 0xB2AA, 0x0000, true, true);
        public static Crc16Model CRC16_T10_DIF = new Crc16Model(0x8BB7, 0x0000, 0x0000, false, false);
        public static Crc16Model CRC16_TELEDISK = new Crc16Model(0xA097, 0x0000, 0x0000, false, false);
        public static Crc16Model CRC16_TMS37157 = new Crc16Model(0x1021, 0x89EC, 0x0000, true, true);
        public static Crc16Model CRC16_USB = new Crc16Model(0x8005, 0xFFFF, 0xFFFF, true, true);
        public static Crc16Model CRC16_A = new Crc16Model(0x1021, 0xC6C6, 0x0000, true, true);
        public static Crc16Model CRC16_KERMIT = new Crc16Model(0x1021, 0x0000, 0x0000, true, true);
        public static Crc16Model CRC16_MODBUS = new Crc16Model(0x8005, 0xFFFF, 0x0000, true, true);
        public static Crc16Model CRC16_X_25 = new Crc16Model(0x1021, 0xFFFF, 0xFFFF, true, true);
        public static Crc16Model CRC16_XMODEM = new Crc16Model(0x1021, 0x0000, 0x0000, false, false);
    }
}
