﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using SunCRC.Crc.Crc16;
using SunCRC.Crc;

namespace SunCRC.Crc.Crc16
{
    /// <summary>
    /// Different methods to compute a CRC16 value.
    /// </summary>
    public class Crc16
    {
        private Crc16Model crcModel;
        private ushort[] crcTable;

        public Crc16(Crc16Model model)
        {
            this.crcModel = model;
            CalculateCrcTable();
        }

        /// <summary>
        /// General CRC-16 algorithm close to the behavior of a shift register.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public ushort Compute_Simple_ShiftReg(byte[] bytes)
        {
            ushort crc = GetInitialShiftRegister(bytes);
            /* skip first two bytes, already inside crc register */
            for (int byteIndex = 2; byteIndex < bytes.Length + 2; byteIndex++)
            {
                byte curByte = (byteIndex < bytes.Length) ? (crcModel.InputReflected ? CrcUtil.Reflect8(bytes[byteIndex]) : bytes[byteIndex]) : (byte)0;

                for (int i = 7; i >= 0; i--)
                {
                    if ((crc & 0x8000) != 0)
                    {
                        crc = (ushort)(crc << 1);
                        crc = ((ushort)(curByte & (1 << i)) != 0) ? (ushort)(crc | 0x0001) : (ushort)(crc & 0xFFFE);
                        crc = (ushort)(crc ^ crcModel.Polynomial);
                    }
                    else
                    {
                        crc = (ushort)(crc << 1);
                        crc = ((ushort)(curByte & (1 << i)) != 0) ? (ushort)(crc | 0x0001) : (ushort)(crc & 0xFFFE);
                    }
                }
            }
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect16(crc) : crc);
            return (ushort)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// Calculate the initial value of the left shift register
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private ushort GetInitialShiftRegister(byte[] bytes)
        {
            byte b1, b2;

            // fill the register with the initial value ^ the first two bytes of the input stream
            if (bytes.Length >= 2)
            {
                b1 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[0]) : bytes[0];
                b2 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[1]) : bytes[1];
            }
            else if (bytes.Length == 1)
            {
                b1 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[0]) : bytes[0];
                b2 = 0;
            }
            else
            {
                b1 = 0;
                b2 = 0;
            }

            return (ushort)(crcModel.Initial ^ (ushort)((ushort)b1 << 8 | b2)); 
        }

        /// <summary>
        /// General CRC-16 simple algorithm.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public ushort Compute_Simple(byte[] bytes)
        {
            ushort crc = crcModel.Initial;

            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);
                crc ^= (ushort)(curByte << 8); /* move byte into MSB of 16bit CRC */

                for (int i = 0; i < 8; i++)
                {
                    if ((crc & 0x8000) != 0)
                    {
                        crc = (ushort)((crc << 1) ^ crcModel.Polynomial);
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }

            crc = (crcModel.ResultReflected ? CrcUtil.Reflect16(crc) : crc);
            return (ushort)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// Precalculate CRC16 look up table.
        /// </summary>
        private void CalculateCrcTable()
        {
            crcTable = new ushort[256];

            for (int divident = 0; divident < 256; divident++)
            {
                ushort curByte = (ushort)(divident << 8);
                for (byte bit = 0; bit < 8; bit++)
                {
                    if ((curByte & 0x8000) != 0)
                    {
                        curByte <<= 1;
                        curByte ^= crcModel.Polynomial;
                    }
                    else
                    {
                        curByte <<= 1;
                    }
                }

                crcTable[divident] = curByte;
            }
        }

        /// <summary>
        /// Calculate CRC16 using lookup table.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public ushort Compute(byte[] bytes)
        {
            ushort crc = crcModel.Initial;
            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);
                /* update the MSB of crc value with next input byte */
                crc = (ushort)(crc ^ (curByte << 8));
                /* this MSB byte value is the index into the lookup table */
                byte pos = (byte)(crc >> 8);
                /* shift out this index */
                crc = (ushort)(crc << 8);
                /* XOR-in remainder from lookup table using the calculated index */
                crc = (ushort)(crc ^ (ushort)crcTable[pos]);

                /* shorter:
                byte pos = (byte)((crc ^ (curByte << 8)) >> 8); 
                crc = (ushort)((crc << 8) ^ (ushort)(crcTable[pos]));
                */
            }
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect16(crc) : crc);
            return (ushort)(crc ^ crcModel.FinalXor);
        }

        public ReadOnlyCollection<ushort> GetCrcTable()
        {
            return Array.AsReadOnly(crcTable);
        }
    }
}
