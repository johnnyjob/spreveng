﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc16
{
    /// <summary>
    /// Representing all required values to define a CRC-16
    /// </summary>
    public class Crc16Model
    {
        public ushort Polynomial { get; private set; }
        public ushort Initial { get; private set; }
        public ushort FinalXor { get; private set; }
        public bool InputReflected { get; private set; }
        public bool ResultReflected { get; private set; }

        public Crc16Model(ushort polynomial, ushort initialVal, ushort finalXorVal, bool inputReflected, bool resultReflected)
        {
            this.Polynomial = polynomial;
            this.Initial = initialVal;
            this.FinalXor = finalXorVal;
            this.InputReflected = inputReflected;
            this.ResultReflected = resultReflected;
        }

        public static Crc16Model GetReflectedCrcModel(Crc16Model model)
        {
            return new Crc16Model(
                CrcUtil.Reflect16(model.Polynomial),
                CrcUtil.Reflect16(model.Initial),
                model.FinalXor,
                !model.InputReflected,
                !model.ResultReflected
            );
        }
    }
}
