﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using SunCRC.Crc.Crc16;
using SunCRC.Crc;

namespace SunCRC.Crc.Crc16
{
    /// <summary>
    /// Different methods to compute a CRC16 value in the reflected way.
    /// </summary>
    public class Crc16Reflected
    {
        private Crc16Model crcModel;
        private ushort[] crcTable;

        public Crc16Reflected(Crc16Model model)
        {
            this.crcModel = Crc16Model.GetReflectedCrcModel(model);

            CalculateCrcTable();
        }

        /// <summary>
        /// General CRC-16 algorithm close to the behavior of a shift register.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public ushort Compute_Simple_ShiftReg(byte[] bytes)
        {
            ushort crc = GetInitialShiftRegister(bytes);

            /* skip first two bits, already inside crc register */
            for (int byteIndex = 2; byteIndex < bytes.Length + 2; byteIndex++)
            {
                byte curByte = (byteIndex < bytes.Length) ? (crcModel.InputReflected ? CrcUtil.Reflect8(bytes[byteIndex]) : bytes[byteIndex]) : (byte)0;

                for (int i = 0; i <= 7; i++)
                {
                    if ((crc & 0x0001) != 0)
                    {
                        crc = (ushort)(crc >> 1);
                        crc = ((ushort)(curByte & (1 << i)) != 0) ? (ushort)(crc | 0x8000) : (ushort)(crc & 0x7FFF);
                        crc = (ushort)(crc ^ crcModel.Polynomial);
                    }
                    else
                    {
                        crc = (ushort)(crc >> 1);
                        crc = ((ushort)(curByte & (1 << i)) != 0) ? (ushort)(crc | 0x8000) : (ushort)(crc & 0x7FFF);
                    }
                }
            }

            crc = (crcModel.ResultReflected ? CrcUtil.Reflect16(crc) : crc);
            return (ushort)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// Calculate the initial value of the left shift register
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private ushort GetInitialShiftRegister(byte[] bytes)
        {
            byte b1, b2;

            // fill the register with the initial value ^ the first two bytes of the input stream
            if (bytes.Length >= 2)
            {
                b1 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[0]) : bytes[0];
                b2 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[1]) : bytes[1];
            }
            else if (bytes.Length == 1)
            {
                b1 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[0]) : bytes[0];
                b2 = 0;
            }
            else
            {
                b1 = 0;
                b2 = 0;
            }

            return (ushort)(crcModel.Initial ^ (ushort)((ushort)b2 << 8 | b1));
        }

        /// <summary>
        /// General CRC-16 simple algorithm.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public ushort Compute_Simple(byte[] bytes)
        {
            ushort crc = crcModel.Initial;

            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);
                crc ^= (ushort)(curByte); /* move byte into LSB of 16bit CRC */

                for (int i = 0; i < 8; i++)
                {
                    if ((crc & 0x0001) != 0)
                    {
                        crc = (ushort)((crc >> 1) ^ crcModel.Polynomial);
                    }
                    else
                    {
                        crc >>= 1;
                    }
                }
            }

            crc = (crcModel.ResultReflected ? CrcUtil.Reflect16(crc) : crc);
            return (ushort)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// Precalculate CRC16 look up table.
        /// </summary>
        private void CalculateCrcTable()
        {
            crcTable = new ushort[256];

            for (int divident = 0; divident < 256; divident++)
            {
                ushort curByte = (ushort)(divident);

                for (byte bit = 0; bit < 8; bit++)
                {
                    if ((curByte & 0x0001) != 0)
                    {
                        curByte >>= 1;
                        curByte ^= crcModel.Polynomial;
                    }
                    else
                    {
                        curByte >>= 1;
                    }
                }

                crcTable[divident] = curByte;
            }
        }

        /* Keep other direction, but reflect processed byte. */
        /*
        public void CalculateCrcTable_AlternateImplementation()
        {
            crcTable = new ushort[256];

            for (int divident = 0; divident < 256; divident++)
            {
                byte reflected = CrcUtil.Reflect8((byte)divident);

                ushort curByte = (ushort)(reflected << 8);

                for (byte bit = 0; bit < 8; bit++)
                {
                    if ((curByte & 0x8000) != 0)
                    {
                        curByte <<= 1;
                        curByte ^= crcModel.Polynomial;
                    }
                    else
                    {
                        curByte <<= 1;
                    }
                }

                curByte = CrcUtil.Reflect16(curByte);

                crcTable[divident] = curByte;
            }
        }*/

        /// <summary>
        /// Calculate CRC16 using lookup table.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public ushort Compute(byte[] bytes)
        {
            ushort crc = crcModel.Initial;
            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);

                /* update the LSB of crc value with next input byte */
                crc = (ushort)(crc ^ curByte);
                /* this value is the index into the lookup table, make sure it's a byte value */
                byte pos = (byte)(crc & 0xFF);
                /* shift out this index */
                crc = (ushort)(crc >> 8);
                /* XOR-in remainder from lookup table using the calculated index */
                crc = (ushort)(crc ^ (ushort)crcTable[pos]);

                /* shorter:
                byte pos = (byte)((crc ^ curByte) & 0xFF);
                crc = (ushort)((crc >> 8) ^ (ushort)(crcTable[pos]));
                */
            }
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect16(crc) : crc);
            return (ushort)(crc ^ crcModel.FinalXor);
        }

        public ReadOnlyCollection<ushort> GetCrcTable()
        {
            return Array.AsReadOnly(crcTable);
        }
    }
}
