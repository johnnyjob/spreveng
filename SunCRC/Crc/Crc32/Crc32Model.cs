﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc32
{
    public class Crc32Model
    {
        public uint Polynomial { get; private set; }
        public uint Initial { get; private set; }
        public uint FinalXor { get; private set; }
        public bool InputReflected { get; private set; }
        public bool ResultReflected { get; private set; }

        public Crc32Model(uint polynomial, uint initialVal, uint finalXorVal, bool inputReflected, bool resultReflected)
        {
            this.Polynomial = polynomial;
            this.Initial = initialVal;
            this.FinalXor = finalXorVal;
            this.InputReflected = inputReflected;
            this.ResultReflected = resultReflected;
        }

        public static Crc32Model GetReflectedCrcModel(Crc32Model model)
        {
            return new Crc32Model(
                CrcUtil.Reflect32(model.Polynomial),
                CrcUtil.Reflect32(model.Initial),
                model.FinalXor,
                !model.InputReflected,
                !model.ResultReflected
            );
        }
    }
}
