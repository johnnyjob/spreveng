﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc32
{
    class Crc32
    {
        private readonly uint TOPBIT_MASK = 0x80000000;
        private Crc32Model crcModel;
        private uint[] crcTable;

        public Crc32(Crc32Model model)
        {
            this.crcModel = model;
            CalculateCrcTable();
        }

        /// <summary>
        /// General CRC-32 simple algorithm.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public uint Compute_Simple(byte[] bytes)
        {
            uint crc = crcModel.Initial;

            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);
                crc ^= (uint)(curByte << 24); /* move byte into MSB of 32bit CRC */

                for (int i = 0; i < 8; i++)
                {
                    if ((crc & TOPBIT_MASK) != 0)
                    {
                        crc = (uint)((crc << 1) ^ crcModel.Polynomial);
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }

            crc = (crcModel.ResultReflected ? CrcUtil.Reflect32(crc) : crc);
            return (uint)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// Precalculate CRC32 look up table.
        /// </summary>
        public void CalculateCrcTable()
        {
            crcTable = new uint[256];

            for (int divident = 0; divident < 256; divident++)
            {
                uint curByte = (uint)(divident << 24);
                for (byte bit = 0; bit < 8; bit++)
                {
                    if ((curByte & TOPBIT_MASK) != 0)
                    {
                        curByte <<= 1;
                        curByte ^= crcModel.Polynomial;
                    }
                    else
                    {
                        curByte <<= 1;
                    }
                }

                crcTable[divident] = curByte;
            }
        }

        /// <summary>
        /// Calculate CRC32 using lookup table.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public uint Compute(byte[] bytes)
        {
            uint crc = crcModel.Initial;
            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);
                
                /* update the MSB of crc value with next input byte */
                crc = (uint)(crc ^ (curByte << 24));
                /* this MSB byte value is the index into the lookup table */
                byte pos = (byte)(crc >> 24);
                /* shift out this index */
                crc = (uint)(crc << 8);
                /* XOR-in remainder from lookup table using the calculated index */
                crc = (uint)(crc ^ (uint)crcTable[pos]);

                /* shorter:
                byte pos = (byte)((crc ^ (curByte << 24)) >> 24);
                crc = (uint)((crc << 8) ^ (uint)(crcTable[pos]));
                */
            }
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect32(crc) : crc);
            return (uint)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// General CRC-16 algorithm close to the behavior of a shift register.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public uint Compute_Simple_ShiftReg(byte[] bytes)
        {
            uint crc = GetInitialShiftRegister(bytes);
            /* skip first four bytes, already inside crc register */
            for (int byteIndex = 4; byteIndex < bytes.Length + 4; byteIndex++)
            {
                byte curByte = (byteIndex < bytes.Length) ? (crcModel.InputReflected ? CrcUtil.Reflect8(bytes[byteIndex]) : bytes[byteIndex]) : (byte)0;

                for (int i = 7; i >= 0; i--)
                {
                    if ((crc & TOPBIT_MASK) != 0)
                    {
                        crc = (uint)(crc << 1);
                        crc = ((uint)(curByte & (1 << i)) != 0) ? (uint)(crc | 0x00000001) : (uint)(crc & 0xFFFFFFFE);
                        crc = (uint)(crc ^ crcModel.Polynomial);
                    }
                    else
                    {
                        crc = (uint)(crc << 1);
                        crc = ((uint)(curByte & (1 << i)) != 0) ? (uint)(crc | 0x00000001) : (uint)(crc & 0xFFFFFFFE);
                    }
                }
            }
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect32(crc) : crc);
            return (uint)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// Calculate the initial value of the left shift register
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private uint GetInitialShiftRegister(byte[] bytes)
        {
            byte b1 = 0, b2 = 0, b3 = 0, b4 = 0;
            // fill the register with the initial value ^ the first two bytes of the input stream
            if (bytes.Length >= 1)
            {
                b1 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[0]) : bytes[0];
            }

            if (bytes.Length >= 2)
            {
                b2 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[1]) : bytes[1];
            }

            if (bytes.Length >= 3)
            {
                b3 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[2]) : bytes[2];
            }

            if (bytes.Length >= 4)
            {
                b4 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[3]) : bytes[3];
            }

            return (uint)(crcModel.Initial ^ (uint)((uint)b1 << 24 | (uint)b2 << 16 | (uint)b3 << 8 | b4));
        }
    }
}
