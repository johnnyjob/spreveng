﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc32
{
    class Crc32Database
    {
        public static Crc32Model CRC32 = new Crc32Model(0x04C11DB7, 0xFFFFFFFF, 0xFFFFFFFF, true, true);
        public static Crc32Model CRC32_BZIP2 = new Crc32Model(0x04C11DB7, 0xFFFFFFFF, 0xFFFFFFFF, false, false);
        public static Crc32Model CRC32_C = new Crc32Model(0x1EDC6F41, 0xFFFFFFFF, 0xFFFFFFFF, true, true);
        public static Crc32Model CRC32_D = new Crc32Model(0xA833982B, 0xFFFFFFFF, 0xFFFFFFFF, true, true);
        public static Crc32Model CRC32_MPEG2 = new Crc32Model(0x04C11DB7, 0xFFFFFFFF, 0x00000000, false, false);
        public static Crc32Model CRC32_POSIX = new Crc32Model(0x04C11DB7, 0x00000000, 0xFFFFFFFF, false, false);
        public static Crc32Model CRC32_Q = new Crc32Model(0x814141AB, 0x00000000, 0x00000000, false, false);
        public static Crc32Model CRC32_JAMCRC = new Crc32Model(0x04C11DB7, 0xFFFFFFFF, 0x00000000, true, true);
        public static Crc32Model CRC32_XFER = new Crc32Model(0x000000AF, 0x00000000, 0x00000000, false, false);
    }
}
