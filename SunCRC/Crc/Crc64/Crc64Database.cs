﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc64
{
    /// <summary>
    /// Most information taken from http://reveng.sourceforge.net/crc-catalogue/
    /// </summary>
    class Crc64Database
    {
        public static Crc64Model CRC64 = new Crc64Model(0x42F0E1EBA9EA3693, 0x00, 0x00, false, false);
        public static Crc64Model CRC64_GO_ISO = new Crc64Model(0x000000000000001B, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, true, true);
        public static Crc64Model CRC64_WE = new Crc64Model(0x42F0E1EBA9EA3693, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, false, false);
        public static Crc64Model CRC64_XZ = new Crc64Model(0x42F0E1EBA9EA3693, 0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF, true, true);
    }
}
