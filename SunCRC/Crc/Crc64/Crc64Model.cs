﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc64
{
    public class Crc64Model
    {
        public ulong Polynomial { get; private set; }
        public ulong Initial { get; private set; }
        public ulong FinalXor { get; private set; }
        public bool InputReflected { get; private set; }
        public bool ResultReflected { get; private set; }

        public Crc64Model(ulong polynomial, ulong initialVal, ulong finalXorVal, bool inputReflected, bool resultReflected)
        {
            this.Polynomial = polynomial;
            this.Initial = initialVal;
            this.FinalXor = finalXorVal;
            this.InputReflected = inputReflected;
            this.ResultReflected = resultReflected;
        }

        public static Crc64Model GetReflectedCrcModel(Crc64Model model)
        {
            return new Crc64Model(
                CrcUtil.Reflect64(model.Polynomial),
                CrcUtil.Reflect64(model.Initial),
                model.FinalXor,
                !model.InputReflected,
                !model.ResultReflected
            );
        }
    }
}
