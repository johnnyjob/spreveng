﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc.Crc64
{
    class Crc64
    {
        private readonly ulong TOPBIT_MASK = 0x8000000000000000;
        private Crc64Model crcModel;
        private ulong[] crcTable;

        public Crc64(Crc64Model model)
        {
            this.crcModel = model;
            CalculateCrcTable();
        }

        /// <summary>
        /// General CRC-32 simple algorithm.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public ulong Compute_Simple(byte[] bytes)
        {
            ulong crc = crcModel.Initial;

            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);
                crc ^= (ulong)((ulong)curByte << 56); /* move byte into MSB of 64bit CRC */

                for (int i = 0; i < 8; i++)
                {
                    if ((crc & TOPBIT_MASK) != 0)
                    {
                        crc = (ulong)((crc << 1) ^ crcModel.Polynomial);
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }

            
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect64(crc) : crc);
            return (ulong)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// Precalculate CRC32 look up table.
        /// </summary>
        private void CalculateCrcTable()
        {
            crcTable = new ulong[256];

            for (int divident = 0; divident < 256; divident++)
            {
                ulong curByte = (ulong)((ulong)divident << 56);
                for (byte bit = 0; bit < 8; bit++)
                {
                    if ((curByte & TOPBIT_MASK) != 0)
                    {
                        curByte <<= 1;
                        curByte ^= crcModel.Polynomial;
                    }
                    else
                    {
                        curByte <<= 1;
                    }
                }

                crcTable[divident] = curByte;
            }
        }

        /// <summary>
        /// Calculate CRC32 using lookup table.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public ulong Compute(byte[] bytes)
        {
            ulong crc = crcModel.Initial;
            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);
                
                /* update the MSB of crc value with next input byte */
                crc = (ulong)(crc ^ (ulong)((ulong)curByte << 56));
                /* this MSB byte value is the index into the lookup table */
                byte pos = (byte)(crc >> 56);
                /* shift out this index */
                crc = (ulong)(crc << 8);
                /* XOR-in remainder from lookup table using the calculated index */
                crc = (ulong)(crc ^ (ulong)crcTable[pos]);

                /* shorter:
                byte pos = (byte)((crc ^ (curByte << 56)) >> 56);
                crc = (uint)((crc << 8) ^ (ulong)(crcTable[pos]));
                */
            }
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect64(crc) : crc);
            return (ulong)(crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// General CRC-16 algorithm close to the behavior of a shift register.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public ulong Compute_Simple_ShiftReg(byte[] bytes)
        {
            ulong crc = GetInitialShiftRegister(bytes);
            /* skip first eight bytes, already inside crc register */
            for (int byteIndex = 8; byteIndex < bytes.Length + 8; byteIndex++)
            {
                byte curByte = (byteIndex < bytes.Length) ? (crcModel.InputReflected ? CrcUtil.Reflect8(bytes[byteIndex]) : bytes[byteIndex]) : (byte)0;

                for (int i = 7; i >= 0; i--)
                {
                    if ((crc & TOPBIT_MASK) != 0)
                    {
                        crc = (ulong)(crc << 1);
                        crc = ((ulong)(curByte & (1 << i)) != 0) ? (ulong)(crc | 0x0000000000000001) : (ulong)(crc & 0xFFFFFFFFFFFFFFFE);
                        crc = (ulong)(crc ^ crcModel.Polynomial);
                    }
                    else
                    {
                        crc = (ulong)(crc << 1);
                        crc = ((ulong)(curByte & (1 << i)) != 0) ? (ulong)(crc | 0x0000000000000001) : (ulong)(crc & 0xFFFFFFFFFFFFFFFE);
                    }
                }
            }
            crc = (crcModel.ResultReflected ? CrcUtil.Reflect64(crc) : crc);
            return (ulong)((ulong)crc ^ crcModel.FinalXor);
        }

        /// <summary>
        /// Calculate the initial value of the left shift register
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private ulong GetInitialShiftRegister(byte[] bytes)
        {
            byte b1 = 0, b2 = 0, b3 = 0, b4 = 0, b5 = 0, b6 = 0, b7 = 0, b8 = 0;
            // fill the register with the initial value ^ the first two bytes of the input stream
            if (bytes.Length >= 1)
            {
                b1 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[0]) : bytes[0];
            }

            if (bytes.Length >= 2)
            {
                b2 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[1]) : bytes[1];
            }

            if (bytes.Length >= 3)
            {
                b3 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[2]) : bytes[2];
            }

            if (bytes.Length >= 4)
            {
                b4 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[3]) : bytes[3];
            }

            if (bytes.Length >= 5)
            {
                b5 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[4]) : bytes[4];
            }

            if (bytes.Length >= 6)
            {
                b6 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[5]) : bytes[5];
            }

            if (bytes.Length >= 7)
            {
                b7 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[6]) : bytes[6];
            }

            if (bytes.Length >= 8)
            {
                b8 = crcModel.InputReflected ? CrcUtil.Reflect8(bytes[7]) : bytes[7];
            }

            return (ulong)(crcModel.Initial ^ (ulong)((ulong)b1 << 56 | (ulong)b2 << 48 | (ulong)b3 << 40 | (ulong)b4 << 32 |
                                                      (ulong)b5 << 24 | (ulong)b6 << 16 | (ulong)b7 << 8  | (ulong)b8 << 0 ));
        }
    }
}
