﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.Crc
{
    public class CrcUtil
    {
        public static byte Reflect8(byte val)
        {
            byte resByte = 0;

            for (int i = 0; i < 8; i++)
            {
                if ((val & (1 << i)) != 0)
                {
                    resByte |= (byte)(1 << (7 - i));
                }
            }

            return resByte;
        }

        public static ushort Reflect16(ushort val)
        {
            ushort resVal = 0;

            for (int i = 0; i < 16; i++)
            {
                if ((val & (1 << i)) != 0)
                {
                    resVal |= (ushort)(1 << (15 - i));
                }
            }

            return resVal;
        }

        public static uint Reflect32(uint val)
        {
            uint resVal = 0;

            for (int i = 0; i < 32; i++)
            {
                if ((val & (1 << i)) != 0)
                {
                    resVal |= (uint)(1 << (31 - i));
                }
            }

            return resVal;
        }

        public static ulong Reflect64(ulong val)
        {
            ulong resVal = 0;

            for (int i = 0; i < 64; i++)
            {
                if ((val & ((ulong)1 << i)) != 0)
                {
                    resVal |= ((ulong)1 << (63 - i));
                }
            }

            return resVal;
        }

        public static uint ReflectGeneral(uint val, int len)
        {
            if (len > 32)
                throw new ArgumentException("Parameter len too large. Only 32bit or less supported.");

            uint resByte = 0;

            for (int i = 0; i < len; i++)
            {
                if ((val & (1 << i)) != 0)
                {
                    resByte |= (uint)(1 << ((len - 1) - i));
                }
            }

            return resByte;
        }
    }
}
