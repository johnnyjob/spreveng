﻿/*
 * Implementations of different CRC algorithms for CRC-8, CRC-16 and CRC-32.
 * 
 * Implementations variants: Shift register, Simple byte-wise, (Lookup) Table-based
 * 
 * Crc package: Standard CRC implementations (simple and table-based)
 * CrcGeneric package: Generic (not type-based) CRC implementation
 * CrcTest: test package to validate all CRC implementations.
 * 
 * Version history:
 * ----------------
 * December 2017:
 * - Added CRC64
 *
 * November 2016:
 * - Added reflected versions
 * 
 * January 2015:
 * - Initial version
 * 
 * Sunshine, December 2017
 * 
 * www.sunshine2k.de || www.bastian-molkenthin.de
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunCRC.CRCGeneric;
using SunCRC.CrcTest;
using SunCRC.CrcTest.Crc8Test;
using SunCRC.CrcTest.Crc16Test;
using SunCRC.CrcTest.Crc32Test;
using SunCRC.CrcTest.Crc64Test;
using SunCRC.CrcGeneric;
using SunCRC.CRCGeneric.BaseGeneric;
using SunCRC.Crc.Crc8;
using SunCRC.Crc.Crc16;
using SunCRC.Crc;
using System.Collections.ObjectModel;

namespace SunCRC
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Out.WriteLine("===============================================");
            Console.Out.WriteLine(" CRC Algorithms");
            Console.Out.WriteLine(" www.sunshine2k.de || www.bastian-molkenthin.de");
            Console.Out.WriteLine("===============================================");

            RunCrcTests();

            Console.In.ReadLine();
        }

        private static void RunCrcTests()
        {
            RunCrc8Tests();
            Console.Out.WriteLine("\n\n");
            RunCrc16Tests();
            Console.Out.WriteLine("\n\n");
            RunCrc32Tests();
            Console.Out.WriteLine("\n\n");
            RunCrc64Tests();
            Console.Out.WriteLine("\n");
        }

        private static void RunCrc8Tests()
        {
            Console.Out.WriteLine("CRC8 Tests");
            Console.Out.WriteLine("----------");
            Crc8Test crc8Test = new Crc8Test();
            crc8Test.RunTests();
        }

        private static void RunCrc16Tests()
        {
            Console.Out.WriteLine("CRC16 Tests");
            Console.Out.WriteLine("----------");
            Crc16Test crc16Test = new Crc16Test();
            crc16Test.RunTests();
            
        }

        private static void RunCrc32Tests()
        {
            Console.Out.WriteLine("CRC32 Tests");
            Console.Out.WriteLine("----------");
            Crc32Test crc32Test = new Crc32Test();
            crc32Test.RunTests();
        }

        private static void RunCrc64Tests()
        {
            Console.Out.WriteLine("CRC64 Tests");
            Console.Out.WriteLine("----------");
            Crc64Test crc64Test = new Crc64Test();
            crc64Test.RunTests();
        }
    }
}
