﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunCRC.CRCGeneric.BaseGeneric;
using SunCRC.Crc;

namespace SunCRC.CrcGeneric
{
    /// <summary>
    /// CRC algorithm working with generic data types, thus only one algorithm implementatin for CRC-8, CRC-16 and CRC-32.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class CrcGenericImpl<T>
    {
        private CrcModelGeneric<T> crcModel;
        private CrcBaseValues<T> baseType;

        public CrcGenericImpl(CrcModelGeneric<T> model, CrcBaseValues<T> baseType)
        {
            this.crcModel = model;
            this.baseType = baseType;
        }

        public T Compute_Simple(byte[] bytes)
        {
            Number<T> crc = crcModel.Initial;

            foreach (byte b in bytes)
            {
                byte curByte = (crcModel.InputReflected ? CrcUtil.Reflect8(b) : b);
                crc ^= Number<T>.LeftShift((int)curByte, (baseType.WidthInBits - baseType.Eight)); /* move byte into MSB of CRC */

                for (int i = 0; i < 8; i++)
                {
                    if ((crc & baseType.MostSignificantBitMask) != baseType.Zero)
                    {
                        crc = (Number<T>.LeftShift(crc, baseType.One) ^ crcModel.Polynomial);
                    }
                    else
                    {
                        crc = Number<T>.LeftShift(crc, baseType.One);
                    }
                }
            }

            crc = (crcModel.ResultReflected ? Number<T>.Reflect(crc) : crc);
            return (crc ^ crcModel.FinalXor);

        }

       
    }
}
