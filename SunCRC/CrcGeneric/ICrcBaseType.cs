﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunCRC.CRCGeneric.BaseGeneric;

namespace SunCRC.CrcGeneric
{
    public class CrcBaseValues<T>
    {
        public Number<T> MostSignificantBitMask { get; private set; }
        public Number<T> WidthInBits { get; private set; }
        public Number<T> Zero { get; private set; }
        public Number<T> One { get; private set; }
        public Number<T> Eight { get; private set; }

        public CrcBaseValues(T MostSignificantBitMask, T WidthInBits, T Zero, T One, T Eight)
        {
            this.MostSignificantBitMask = MostSignificantBitMask;
            this.WidthInBits = WidthInBits;
            this.Zero = Zero;
            this.One = One;
            this.Eight = Eight;
        }
    }
}
