﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunCRC.CRCGeneric.BaseGeneric;

namespace SunCRC.CrcGeneric
{
    class CrcModelGeneric<T>
    {
        public Number<T> Polynomial { get; private set; }
        public Number<T> Initial { get; private set; }
        public Number<T> FinalXor { get; private set; }
        public bool InputReflected { get; private set; }
        public bool ResultReflected { get; private set; }

        public CrcModelGeneric(T polynomial, T initialVal, T finalXorVal, bool inputReflected, bool resultReflected)
        {
            this.Polynomial = polynomial;
            this.Initial = initialVal;
            this.FinalXor = finalXorVal;
            this.InputReflected = inputReflected;
            this.ResultReflected = resultReflected;
        }
    }
}
