﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunCRC.Crc.Crc8;
using SunCRC.CRCGeneric.BaseGeneric;
using SunCRC.Crc.Crc16;
using SunCRC.Crc.Crc32;
using SunCRC.Crc.Crc64;

namespace SunCRC.CrcGeneric
{
    class CrcGenericFactory
    {
        private static CrcBaseValues<byte> crcBaseValues8 = null;
        private static CrcBaseValues<ushort> crcBaseValues16 = null;
        private static CrcBaseValues<uint> crcBaseValues32 = null;
        private static CrcBaseValues<ulong> crcBaseValues64 = null;

        private static CrcBaseValues<byte> GetCrcBaseValues8()
        {
            if (crcBaseValues8 == null)
            {
                crcBaseValues8 = new CrcBaseValues<byte>(0x80, 8, 0, 1, 8);
            }
            return crcBaseValues8;
        }

        private static CrcBaseValues<ushort> GetCrcBaseValues16()
        {
            if (crcBaseValues16 == null)
            {
                crcBaseValues16 = new CrcBaseValues<ushort>(0x8000, 16, 0, 1, 8);
            }
            return crcBaseValues16;
        }

        private static CrcBaseValues<uint> GetCrcBaseValues32()
        {
            if (crcBaseValues32 == null)
            {
                crcBaseValues32 = new CrcBaseValues<uint>(0x80000000, 32, 0, 1, 8);
            }
            return crcBaseValues32;
        }

        private static CrcBaseValues<ulong> GetCrcBaseValues64()
        {
            if (crcBaseValues64 == null)
            {
                crcBaseValues64 = new CrcBaseValues<ulong>(0x8000000000000000, 64, 0, 1, 8);
            }
            return crcBaseValues64;
        }

        public static CrcGenericImpl<byte> GetGenericCrc8(Crc8Model model)
        {
            CrcModelGeneric<byte> genModel = new CrcModelGeneric<byte>(model.Polynomial, model.Initial, model.FinalXor,
                                model.InputReflected, model.ResultReflected);
            return new CrcGenericImpl<byte>(genModel, GetCrcBaseValues8());
        }

        public static CrcGenericImpl<ushort> GetGenericCrc16(Crc16Model model)
        {
            CrcModelGeneric<ushort> genModel = new CrcModelGeneric<ushort>(model.Polynomial, model.Initial, model.FinalXor,
                                model.InputReflected, model.ResultReflected);
            return new CrcGenericImpl<ushort>(genModel, GetCrcBaseValues16());
        }

        public static CrcGenericImpl<uint> GetGenericCrc32(Crc32Model model)
        {
            CrcModelGeneric<uint> genModel = new CrcModelGeneric<uint>(model.Polynomial, model.Initial, model.FinalXor,
                                model.InputReflected, model.ResultReflected);
            return new CrcGenericImpl<uint>(genModel, GetCrcBaseValues32());
        }

        public static CrcGenericImpl<ulong> GetGenericCrc64(Crc64Model model)
        {
            CrcModelGeneric<ulong> genModel = new CrcModelGeneric<ulong>(model.Polynomial, model.Initial, model.FinalXor,
                                model.InputReflected, model.ResultReflected);
            return new CrcGenericImpl<ulong>(genModel, GetCrcBaseValues64());
        }
    }
}
