﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.CRCGeneric.BaseGeneric
{
    class UIntCalculator : ICalculator<uint>
    {
        public uint Xor(uint a, uint b)
        {
            return (uint)(a ^ b);
        }

        public uint LeftShift(uint a, uint b)
        {
            return (uint)(a << (int)b);
        }

        public uint LeftShift(uint a, int b)
        {
            return (uint)(a << b);
        }

        public uint LeftShift(int a, uint b)
        {
            return (uint)(a << (int)b);
        }

        public uint Difference(uint a, uint b)
        {
            return (uint)(a - b);
        }

        public uint And(uint a, uint b)
        {
            return (uint)(a & b);
        }

        public bool Equal(uint a, uint b)
        {
            return (a == b);
        }

        public bool Unequal(uint a, uint b)
        {
            return (a != b);
        }

        public uint Reflect(uint a)
        {
            uint resVal = 0;

            for (int i = 0; i < 32; i++)
            {
                if ((a & (1 << i)) != 0)
                {
                    resVal |= (uint)(1 << (31 - i));
                }
            }

            return resVal;
        }
    }
}
