﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.CRCGeneric.BaseGeneric
{
    class UShortCalculator : ICalculator<ushort>
    {
        public ushort Xor(ushort a, ushort b)
        {
            return (ushort)(a ^ b);
        }

        public ushort LeftShift(ushort a, ushort b)
        {
            return (ushort)(a << b);
        }

        public ushort LeftShift(ushort a, int b)
        {
            return (ushort)(a << b);
        }

        public ushort LeftShift(int a, ushort b)
        {
            return (ushort)(a << b);
        }

        public ushort Difference(ushort a, ushort b)
        {
            return (ushort)(a - b);
        }

        public ushort And(ushort a, ushort b)
        {
            return (ushort)(a & b);
        }

        public bool Equal(ushort a, ushort b)
        {
            return (a == b);
        }

        public bool Unequal(ushort a, ushort b)
        {
            return (a != b);
        }

        public ushort Reflect(ushort a)
        {
            ushort resVal = 0;

            for (int i = 0; i < 16; i++)
            {
                if ((a & (1 << i)) != 0)
                {
                    resVal |= (ushort)(1 << (15 - i));
                }
            }

            return resVal;
        }
    }
}
