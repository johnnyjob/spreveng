﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.CRCGeneric.BaseGeneric
{
    public interface ICalculator<T>
    {
        T Xor(T a, T b);
        T LeftShift(T a, T b);
        T LeftShift(int a, T b);
        T LeftShift(T a, int b);
        T Difference(T a, T b);
        T And(T a, T b);

        bool Equal(T a, T b);
        bool Unequal(T a, T b);

        T Reflect(T a);
    }
}
