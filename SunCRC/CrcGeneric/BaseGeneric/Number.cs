﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.CRCGeneric.BaseGeneric
{
    /// <summary>
    /// Most taken from:
    /// http://www.codeproject.com/Articles/33617/Arithmetic-in-Generic-Classes-in-C
    /// Very interesting class for generic primitive type handling!
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Number<T>
    {
        private T value;

        public Number(T value)
        {
            this.value = value;
        }

        private static Type GetCalculatorType()
        {
            Type tType = typeof(T);
            Type calculatorType = null;
            if (tType == typeof(byte))
            {
                calculatorType = typeof(ByteCalculator);
            }
            else if (tType == typeof(ushort))
            {
                calculatorType = typeof(UShortCalculator);
            }
            else if (tType == typeof(uint))
            {
                calculatorType = typeof(UIntCalculator);
            }
            else if (tType == typeof(ulong))
            {
                calculatorType = typeof(ULongCalculator);
            }
            else
            {
                throw new InvalidCastException(String.Format("Unsupported Type- Type {0}" +
                      " does not have a partner implementation of interface " +
                      "ICalculator<T> and cannot be used in generic " +
                      "arithmetic using type Number<T>", tType.Name));
            }
            return calculatorType;
        }

        /// a static field to store the calculator after it is created
        /// this is the caching that is refered to above
        /// </summary>
        private static ICalculator<T> fCalculator = null;

        /// <summary>

        /// Singleton pattern- only one calculator created per type
        /// 
        /// </summary>
        private static ICalculator<T> Calculator
        {
            get
            {
                if (fCalculator == null)
                {
                    MakeCalculator();
                }
                return fCalculator;
            }
        }

        /// <summary>

        /// Here the actual calculator is created using the system activator
        /// </summary>

        private static void MakeCalculator()
        {
            Type calculatorType = GetCalculatorType();
            fCalculator = Activator.CreateInstance(calculatorType) as ICalculator<T>;
        }

        public static T Xor(T a, T b)
        {
            return Calculator.Xor(a, b);
        }

        public static Number<T> operator -(Number<T> a, Number<T> b)
        {
            return Calculator.Difference(a, b);
        }

        public static Number<T> operator ^ (Number<T> a, Number<T> b)
        {
            return Calculator.Xor(a, b);
        }

        /*
         * This implementation
         * public static Number<T> operator <<(Number<T> a, Number<T> b)
         * is not possible as C# requires an int as second argument, so we use a simply a normal function
         */
        public static Number<T> LeftShift(Number<T> a, Number<T> b)
        {
            return Calculator.LeftShift(a, b);
        }

        public static Number<T> LeftShift(int a, Number<T> b)
        {
            return Calculator.LeftShift(a, b);
        }

        public static Number<T> operator &(Number<T> a, Number<T> b)
        {
            return Calculator.And(a, b);
        }

        public static bool operator ==(Number<T> a, Number<T> b)
        {
            return Calculator.Equal(a, b);
        }

        public static bool operator !=(Number<T> a, Number<T> b)
        {
            return Calculator.Unequal(a, b);
        }

        public static Number<T> Reflect(Number<T> a)
        {
            return Calculator.Reflect(a);
        }

        public static implicit operator Number<T>(T a)
        {
            return new Number<T>(a);
        }

        //IMPORTANT:  The implicit operators allows 
        //an object of type Number<T> to be
        //easily and seamlessly wrap an object of type T. 
        public static implicit operator T(Number<T> a)
        {
            return a.value;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
