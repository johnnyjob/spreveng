﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SunCRC.CRCGeneric.BaseGeneric
{
    struct ByteCalculator : ICalculator<byte>
    {
        public byte Xor(byte a, byte b)
        {
 	        return (byte)(a ^ b);
        }

        public byte LeftShift(byte a, int b)
        {
            return (byte)(a << b);
        }

        public byte LeftShift(byte a, byte b)
        {
            return (byte)(a << b);
        }

        public byte LeftShift(int a, byte b)
        {
            return (byte)(a << b);
        }

        public byte Difference(byte a, byte b)
        {
            return (byte)(a - b);
        }

        public byte And(byte a, byte b)
        {
            return (byte)(a & b);
        }

        public bool Equal(byte a, byte b)
        {
            return (a == b);
        }

        public bool Unequal(byte a, byte b)
        {
            return (a != b);
        }

        public byte Reflect(byte a)
        {
            byte resByte = 0;

            for (int i = 0; i < 8; i++)
            {
                if ((a & (1 << i)) != 0)
                {
                    resByte |= (byte)(1 << (7 - i));
                }
            }

            return resByte;
        }
    }
}
