﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SunCRC.CRCGeneric.BaseGeneric;

namespace SunCRC.CRCGeneric.BaseGeneric
{
    class ULongCalculator : ICalculator<ulong>
    {
        public ulong Xor(ulong a, ulong b)
        {
            return (ulong)(a ^ b);
        }

        public ulong LeftShift(ulong a, ulong b)
        {
            return (ulong)((ulong)a << (int)b);
        }

        public ulong LeftShift(ulong a, int b)
        {
            return (ulong)((ulong)a << b);
        }

        public ulong LeftShift(int a, ulong b)
        {
            return (ulong)((ulong)a << (int)b);
        }

        public ulong Difference(ulong a, ulong b)
        {
            return (ulong)(a - b);
        }

        public ulong And(ulong a, ulong b)
        {
            return (ulong)(a & b);
        }

        public bool Equal(ulong a, ulong b)
        {
            return (a == b);
        }

        public bool Unequal(ulong a, ulong b)
        {
            return (a != b);
        }

        public ulong Reflect(ulong a)
        {
            ulong resVal = 0;

            for (int i = 0; i < 64; i++)
            {
                if ((a & ((ulong)1 << i)) != 0)
                {
                    resVal |= (ulong)((ulong)1 << (63 - i));
                }
            }

            return resVal;
        }
    }
}
