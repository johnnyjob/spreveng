﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrcHack
{
    public class BinarySample
    {
        public byte[] Input;
        public uint Hash;

        public BinarySample(byte[] input, uint hash)
        {
            Input = input;
            Hash = hash;
        }

        public BinarySample(Sample sample, byte[] input)
        {
            Input = input;
            Hash = (uint)sample.Hash;
        }
    }
}
