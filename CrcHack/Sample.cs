﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrcHack
{
    public class Sample
    {
        public Sample(string inputString, int hash)
        {
            InputString = inputString;
            Hash = (uint)hash;
        }

        public string InputString;
        public uint Hash;
    }
}
