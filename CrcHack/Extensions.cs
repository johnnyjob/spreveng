﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrcHack
{
    public static class Extensions
    {
        public static string ToHexString(this byte[] b)
        {
            return BitConverter.ToString(b);
        }

        public static string ToHexString(this int n)
        {
            return n.ToString("X");
        }

        public static string ToHexString(this uint n)
        {
            return n.ToString("X");
        }

        public static string ToBitString(this uint n)
        {
            var bits = Convert.ToString(n, 2).PadLeft(32, '0').ToList();
            int index = 0;
            for (int i = 0; i < 32 / 4 - 1; i++)
            {
                index += 4;
                bits.Insert(index, ' ');
                index++;
            }
            return new string(bits.ToArray());
        }

        public static string ToBitString(this byte n)
        {
            var bits = Convert.ToString(n, 2).PadLeft(8, '0').ToList();
            int index = 0;
            for (int i = 0; i < 8 / 4 - 1; i++)
            {
                index += 4;
                bits.Insert(index, ' ');
                index++;
            }
            return new string(bits.ToArray());
        }

        public static string ToBitString(this int n)
        {
            return ((uint)n).ToBitString();
        }
    }
}
