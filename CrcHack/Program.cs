﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunCRC.Crc;
using SunCRC.Crc.Crc32;

namespace CrcHack
{
    class Program
    {
        private const uint TOPBIT_MASK = 0x80000000;
        private const uint LOWBIT_MASK = 0x00000001;

        private static Sample _a0 = new Sample("http://a0", 2141537348);
        private static Sample _a1 = new Sample("http://a1", -1380154216);
        private static Sample _a2 = new Sample("http://a2", -547891125);
        private static Sample _a4 = new Sample("http://a4", 981557741);
        private static Sample _a8 = new Sample("http://a8", -169013482);
        private static Sample _aq = new Sample("http://aq", -319117100);
        private static Sample _aa = new Sample("http://aa", -55893049);

        private static Sample _aBase =       new Sample("http://a/a", -1668195764);

        private static Sample _httpExclMk =  new Sample("http://a/a!", -727985757);
        private static Sample _httpOpSqBr =  new Sample("http://a/a[", -1886013561);
        private static Sample _httpZ =       new Sample("http://a/az",  2100257149);
        private static Sample _httpOpCrlBr = new Sample("http://a/a{", -1355374175);
        private static Sample _httpX =       new Sample("http://a/ax", -572654222);
        private static Sample _httpTld =     new Sample("http://a/a~",  940262612);
        private static Sample _httpR =       new Sample("http://a/ar", -144233937);

        static void Main(string[] args)
        {
            ResetLog();

            FindPoly();

            var refs = new bool[][]
            {
                new bool[] { true, true },
                new bool[] { false, false },
                new bool[] { true, false },
                new bool[] { false, true }
            };

            var polys = new uint[]
            {
                0x82608EDB,
                0xDB710641,
                0xEDB88320,
                0x04C11DB7
            };
            foreach (uint poly in polys)
            {
                Parallel.ForEach(refs, x => BruteForceXorIn(poly, x[0], x[1]));
            }
        }        

        private static uint FindPoly()
        {
            var enc = Encoding.Unicode;
            var m0 = new BinarySample(_a0, enc.GetBytes(_a0.InputString));
            var m1 = new BinarySample(_a1, enc.GetBytes(_a1.InputString));
            var m2 = new BinarySample(_a2, enc.GetBytes(_a2.InputString));
            var m4 = new BinarySample(_a4, enc.GetBytes(_a4.InputString));
            var m8 = new BinarySample(_a8, enc.GetBytes(_a8.InputString));
            var mq = new BinarySample(_aq, enc.GetBytes(_aq.InputString));
            var ma = new BinarySample(_aa, enc.GetBytes(_aa.InputString));

            uint c0 = 0x00000000;
            var dms = new BinarySample[]
            {
                new BinarySample(Xor(m0.Input, m1.Input), m0.Hash ^ m1.Hash ^ c0),
                new BinarySample(Xor(m0.Input, m2.Input), m0.Hash ^ m2.Hash ^ c0),
                new BinarySample(Xor(m0.Input, m4.Input), m0.Hash ^ m4.Hash ^ c0),
                new BinarySample(Xor(m0.Input, m8.Input), m0.Hash ^ m8.Hash ^ c0),
                new BinarySample(Xor(mq.Input, ma.Input), mq.Hash ^ ma.Hash ^ c0)
            };

            uint result = 0;
            for (int i = 0; i < dms.Length - 1; i++)
            {
                uint poly = ((uint)dms[i + 1].Hash >> 1) ^ (uint)dms[i].Hash;
                Console.WriteLine("Input bytes: {0}, hash: {1}\t poly: {2}",
                    dms[i].Input.ToHexString(), dms[i].Hash.ToHexString(), poly.ToHexString());

                if (poly != 0)
                {
                    if (result != 0 && poly != result)
                    {
                        Console.WriteLine("ERROR: couldn't find generating polynom.");
                    }
                    result = poly;
                }
            }

            return result;
        }

        private static void BruteForceXorIn(uint poly, bool refIn, bool refOut)
        {
            var table = CalculateCrcTable(poly);

            var samples = new Sample[]
            {
                _a0, _a1, _a2, _a4
            };
            var encodings = new Encoding[]
            {
                Encoding.BigEndianUnicode,
                Encoding.UTF8,
                Encoding.Unicode
            };

            var stopwatch = Stopwatch.StartNew();
            foreach (var enc in encodings)
            {
                string logPrefix = String.Format("[{0}/{1}/{2}/{3}]", poly.ToHexString(), refIn, refOut, enc.EncodingName);
                var messages = samples.Select(x => new BinarySample(x, enc.GetBytes(x.InputString))).ToArray();

                uint xorIn = 0;
                do
                {
                    if (xorIn == 0x00000000 || xorIn == 0x10000000 || xorIn == 0x20000000 || xorIn == 0x30000000 ||
                        xorIn == 0x40000000 || xorIn == 0x50000000 || xorIn == 0x60000000 || xorIn == 0x70000000 ||
                        xorIn == 0x80000000 || xorIn == 0x90000000 || xorIn == 0xA0000000 || xorIn == 0xB0000000 ||
                        xorIn == 0xC0000000 || xorIn == 0xD0000000 || xorIn == 0xE0000000 || xorIn == 0xF0000000)
                    {
                        WriteToLog("  {0} xorIn >= {1} time: {2}", logPrefix, xorIn.ToHexString(), stopwatch.Elapsed.ToString("hh\\:mm\\:ss"));
                    }

                    uint[] hashes = messages.Select(x => ComputeCrc(table, xorIn, 0, refIn, refOut, x.Input)).ToArray();
                    var xors = new uint[samples.Length];
                    for (int i = 0; i < samples.Length; i++)
                    {
                        xors[i] = hashes[i] ^ (uint)samples[i].Hash;
                    }

                    if (xors.All(x => x == xors[0]))
                    {
                        WriteToLog("{0} FOUND XorIn: {1}",
                            logPrefix,
                            xorIn.ToHexString());
                    }

                    xorIn++;
                } while (xorIn != 0);
            }
        }

        #region CRC

        private static uint[] CalculateCrcTable(uint poly)
        {
            var crcTable = new uint[256];

            for (int divident = 0; divident < 256; divident++)
            {
                uint curByte = (uint)(divident << 24);
                for (byte bit = 0; bit < 8; bit++)
                {
                    if ((curByte & TOPBIT_MASK) != 0)
                    {
                        curByte <<= 1;
                        curByte ^= poly;
                    }
                    else
                    {
                        curByte <<= 1;
                    }
                }

                crcTable[divident] = curByte;
            }
            return crcTable;
        }

        private static uint ComputeCrc(uint[] crcTable, uint xorIn, uint xorOut, bool refIn, bool refOut, byte[] bytes)
        {
            uint crc = xorIn;
            foreach (byte b in bytes)
            {
                byte curByte = (refIn ? CrcUtil.Reflect8(b) : b);

                /* update the MSB of crc value with next input byte */
                crc = (uint)(crc ^ (curByte << 24));
                /* this MSB byte value is the index into the lookup table */
                byte pos = (byte)(crc >> 24);
                /* shift out this index */
                crc = (uint)(crc << 8);
                /* XOR-in remainder from lookup table using the calculated index */
                crc = (uint)(crc ^ (uint)crcTable[pos]);

                /* shorter:
                byte pos = (byte)((crc ^ (curByte << 24)) >> 24);
                crc = (uint)((crc << 8) ^ (uint)(crcTable[pos]));
                */
            }
            crc = (refOut ? CrcUtil.Reflect32(crc) : crc);
            return (uint)(crc ^ xorOut);
        }

        #endregion

        #region Utilities

        private static byte[] Xor(byte[] x, byte[] y)
        {
            if (x.Length != y.Length)
            {
                throw new ArgumentException("Lengths are not equal");
            }

            var result = new byte[x.Length];
            for (int i = 0; i < x.Length; i++)
            {
                result[i] = (byte)(x[i] ^ y[i]);
            }

            return result;
        }

        private static byte[] Xor(byte[] x, byte[] y, byte[] z)
        {
            if (x.Length != y.Length || x.Length != z.Length)
            {
                throw new ArgumentException("Lengths are not equal");
            }

            var result = new byte[x.Length];
            for (int i = 0; i < x.Length; i++)
            {
                result[i] = (byte)(x[i] ^ y[i] ^ z[i]);
            }

            return result;
        }

        private static void ResetLog()
        {
            File.WriteAllText("out.txt", "");
        }

        private static void WriteToLog(string message, params object[] args)
        {
            Console.WriteLine(message, args);
            File.AppendAllText("out.txt", String.Format(message, args) + "\r\n");
        }

        #endregion
    }
}